<?php

namespace App\Http\Controllers;

use ErrorException;
use Illuminate\Http\Request;

class Tugas2Controller extends Controller
{
    public function index()
    {
        return view('index', ['nama' => 'dwiky']);
    }

    public function kalkulator()
    {
        return view('kalkulator');
    }

    public function hitung(Request $request)
    {
        if ($request['bil1'] === null || $request['bil2'] === null){
            return redirect('/kalkulator')->with(['warning' => 'Input tidak boleh kosong']);
        }

        if($request['bil2'] === '0' && $request['operasi'] == '/'){
            return redirect('/kalkulator')->with(['warning' => 'bilangan pembagi tidak boleh 0']);
        }

        $data = [
            'bil1' => $request['bil1'],
            'operasi' => $request['operasi'],
            'bil2' => $request['bil2']
        ];

        $words = implode(" ", $data);
        $hitung = eval('return '.$words.';');
        $result = $hitung;

        return redirect('/kalkulator')->with(['success' => 'hasil perhitungan: '.$result]);
    }

    public function ganjilGenap()
    {
        return view('/ganjilGenap');
    }

    public function generate(Request $request)
    {
        if($request['bil1'] === null || $request['bil2'] === null){
            return redirect('/ganjilGenap')->with(['warning' => 'bilangan tidak boleh kosong!']);
        }

        $bil1 = $request['bil1'];
        $bil2 = $request['bil2'];
        $awal = $bil1 > $bil2 ? $bil2 : $bil1;
        $akhir = $bil1 > $bil2 ? $bil1 : $bil2;

        $data = [
            'awal' => $awal,
            'akhir' => $akhir
        ];
        
        return view('/generate', $data);
    }

    public function hitungVokal(){
        return view('/hitungVokal');
    }

    public function vokal(Request $request){
        if($request['words'] === null){
            return redirect('/hitungVokal')->with(['warning' => 'Input tidak boleh kosong!']);
        }

        $words = explode(" ", htmlspecialchars($request['words']));
        $vokals = ['a', 'i', 'u', 'e', 'o'];
        $results = [];

        foreach ($words as $word){
            $arr = str_split($word);
            $cekVokal = array_intersect($arr, $vokals);
            $makeUnique = array_unique($cekVokal);
    
            array_push($results, "$word = ".implode(", ",$makeUnique));
        }    

        $data = [
            'array' => $results
        ];

        return view('/vokal', $data);
    }
}
