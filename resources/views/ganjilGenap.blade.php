@extends('templates/layout')

@section('title', 'Ganjil Genap')

@section('container')

<div class="container">
    <div class="row">
        <div class="col">
            <h1>Bilangan Ganjil Genap</h1>

            @if ($message = Session::get('warning'))
            <div class="alert alert-warning alert-block mt-3">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
	        </div>
	        @endif

            <form action="/ganjilGenap" method="POST">
                @csrf
                <div class="form-group">
                    <label for="bil1">Input bilangan pertama</label>
                    <input type="number" name="bil1" class="form-control" id="bil1">
                </div>
                <div class="form-group">
                    <label for="bil2">Input bilangan kedua</label>
                    <input type="number" name="bil2" class="form-control" id="bil2">
                </div>
                <button type="submit" class="btn btn-primary">Generate</button>
            </form>
        </div>
    </div>
</div>

@endsection
