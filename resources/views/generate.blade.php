@extends('templates/layout')

@section('title', 'Ganjil Genap')

@section('container')

<div class="container">
    <div class="row">
        <div class="col">
            <h1>Bilangan Ganjil Genap</h1>
            
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Hasil</th>
                    </tr>
                </thead>
                <tbody>
                    @for ($i = $awal; $i <= $akhir; $i++)
                    
                    @if ($i % 2 === 0)
                    <tr>
                        <td>{{ $i }} adalah bilangan genap</td>
                    </tr>
                    @else
                    <tr>
                        <td>{{ $i }} adalah bilangan ganjil</td>
                    </tr>
                    @endif

                    @endfor
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
