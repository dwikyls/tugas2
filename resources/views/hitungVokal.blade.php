@extends('templates/layout')

@section('title', 'Hitung Huruf Vokal')

@section('container')

<div class="container">
    <div class="row">
        <div class="col">
            <h1>Hitung Huruf Vokal</h1>

            @if ($message = Session::get('warning'))
            <div class="alert alert-warning alert-block mt-3">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
	        </div>
	        @endif
            
            <form action="/hitungVokal" method="POST">
                @csrf
                <div class="form-group">
                    <label for="bil1">Masukan kata atau kalimat</label>
                    <textarea name="words" id="words" class="form-control" cols="30" rows="10"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Hitung</button>
            </form>
        </div>
    </div>
</div>

@endsection
