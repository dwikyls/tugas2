@extends('templates/layout')

@section('title', 'Home')

@section('container')

<div class="container">
    <div class="row">
        <div class="col">
            <h1>Hello {{ $nama }}</h1>
        </div>
    </div>
</div>

@endsection
