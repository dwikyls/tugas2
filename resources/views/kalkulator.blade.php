@extends('templates/layout')

@section('title', 'Kalkulator')

@section('container')

<div class="container">
    <div class="row">
        <div class="col">
            @if ($message = Session::get('warning'))
            <div class="alert alert-warning alert-block mt-3">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
	        </div>
	        @endif

            <h1>Kalkulator Sederhana</h1>
            <form action="/kalkulator" method="POST">
                @csrf
                <div class="form-group">
                    <label for="bil1">Input bilangan pertama</label>
                    <input type="number" name="bil1" class="form-control" id="bil1">
                </div>
                <div class="form-group">
                    <label for="bil2">Input bilangan kedua</label>
                    <input type="number" name="bil2" class="form-control" id="bil2">
                </div>
                <button type="submit" name="operasi" class="btn btn-primary" value="+">+</button>
                <button type="submit" name="operasi" class="btn btn-primary" value="-">-</button>
                <button type="submit" name="operasi" class="btn btn-primary" value="*">*</button>
                <button type="submit" name="operasi" class="btn btn-primary" value="/">/</button>
            </form>

            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block mt-3">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
            </div>
	        @endif
        </div>
    </div>
</div>

@endsection
