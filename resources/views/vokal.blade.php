@extends('templates/layout')

@section('title', 'Hitung Huruf Vokal')

@section('container')

<div class="container">
    <div class="row">
        <div class="col">
            <h1>Hitung Huruf Vokal</h1>
            
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Hasil</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($array as $arr)
                    
                    <tr>
                        <td>{{ $arr }}</td>
                    </tr>

                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
