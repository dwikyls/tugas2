<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Tugas2Controller@index');
Route::get('/kalkulator', 'Tugas2Controller@kalkulator');
Route::post('/kalkulator', 'Tugas2Controller@hitung');
Route::get('/ganjilGenap', 'Tugas2Controller@ganjilGenap');
Route::post('/ganjilGenap', 'Tugas2Controller@generate');
Route::get('/hitungVokal', 'Tugas2Controller@hitungVokal');
Route::post('/hitungVokal', 'Tugas2Controller@vokal');
